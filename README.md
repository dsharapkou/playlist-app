 1. [Introduction](#intro)
 1. [Installation](#install)
 1. [Development mode](#dev)
 
----------
### <a name="intro"></a> Introduction

My Study App is a ReactJS-based web application for my learning of js and libraries, such as ReactJS and Redux.
At this App anyone can observe playlist, filter it by Artist, Genre and Year, also feature of pagination should be.
It will be fully built on ReactJS and later with Redux.

----------
### <a name="install"></a> Installation

1. Clone this project to local environment and switch branch to `develop`
1. [Install Node.js](https://nodejs.org/en/download/)
1. Install all dependencies using `npm run install` command in root folder

----------

### <a name="dev"></a> Development mode
For running app in development mode use next command:

 1. From main folder run`npm start` to start frontend part
