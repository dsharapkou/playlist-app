import React, { Component } from 'react';
import './App.css';
import data from './data.json';
import Playlist from './Playlist.js';


class App extends Component {
    render() {
        return (
            <div className='container'>
                <div className="container">
                    <h1>Welcome to my playlist app!</h1>
                </div>
                <Playlist data={data} />
            </div>
        );
    }
}


export default App;