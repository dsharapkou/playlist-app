import React, { Component } from 'react';
import './App.css';

export default function Playlist(props) {
    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Performer</th>
                        <th>Composition</th>
                        <th>Style</th>
                        <th>Year</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        props.data.map(row => (
                            <tr>
                                <td>{row.Artist}</td>
                                <td>{row.Song}</td>
                                <td>{row.Genre}</td>
                                <td>{row.Year}</td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
        </div>
    );
}





